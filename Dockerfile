FROM alpine

RUN apk update && \
  apk add curl bash bind-tools

COPY livebox-ovh-dyndns.sh /livebox-ovh-dyndns.sh

ENTRYPOINT [ "/livebox-ovh-dyndns.sh" ]