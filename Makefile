CI_REGISTRY_IMAGE ?= livebox-ovh-dyndns
CI_JOB_ID ?= 0
CI_BUILD_TAG ?= dev
CI_COMMIT_SHA ?= $(shell git rev-parse HEAD)
BUILD_VERSION=$(shell git show -s --format=%cd --date=format:%Y%m%d%H%M%S)-$(CI_JOB_ID)
VERSION ?= dev

build:
	docker build -t $(CI_REGISTRY_IMAGE):$(CI_COMMIT_SHA) .

push-build: build
	docker tag $(CI_REGISTRY_IMAGE):$(CI_COMMIT_SHA) $(CI_REGISTRY_IMAGE):$(BUILD_VERSION)
	docker push $(CI_REGISTRY_IMAGE):$(CI_COMMIT_SHA)
	docker push $(CI_REGISTRY_IMAGE):$(BUILD_VERSION)

release:
	docker pull $(CI_REGISTRY_IMAGE):$(CI_COMMIT_SHA)
	docker tag $(CI_REGISTRY_IMAGE):$(CI_COMMIT_SHA) $(CI_REGISTRY_IMAGE):$(VERSION)

push-release: release
	docker push $(CI_REGISTRY_IMAGE):$(VERSION)
