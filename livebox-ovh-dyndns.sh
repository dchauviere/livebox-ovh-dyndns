#! /bin/bash

if [[ "$DYNDNS_HOST" = "" ]] || [[ "$DYNDNS_LOGIN" = "" ]] || [[ "$DYNDNS_PASSWORD" = "" ]]; then
  echo "Variables DYNDNS_HOST, DYNDNS_LOGIN and DYNDNS_PASSWORD need to be defined !"
  exit 1
fi

DYNDNS_LIVEBOX=${LIVEBOX:-livebox}
DYNDNS_WAIT_DURATION=${DURATION:-300}

while true; do
  _ip=$(curl -s -X POST -H "Content-Type: application/json" -d '{"parameters":{}}'  http://$DYNDNS_LIVEBOX/sysbus/NMC:getWANStatus | sed -e 's/.*"IPAddress":"\(.*\)","Remo.*/\1/g')
  _ipv6=$(curl -s -X POST -H "Content-Type: application/json" -d '{"parameters":{}}'  http://$DYNDNS_LIVEBOX/sysbus/NMC:getWANStatus | sed -e 's/.*"IPv6Address":"\(.*\)","IPv6D.*/\1/g')
  _oldip=$(dig +short @$DYNDNS_LIVEBOX $DYNDNS_HOST)

  if [ $_ip ]; then
    printf "$(date) | Old IP: $_oldip, New IP: $_ip => "
    if [ "$_oldip" != "$_ip" ]; then
      _result=$(curl -s -u "$DYNDNS_LOGIN:$DYNDNS_PASSWORD" "http://www.ovh.com/nic/update?system=dyndns&hostname=$DYNDNS_HOST&myip=$_ip")
      if [[ $_result =~ ^(good|nochg).* ]]; then
        echo "Updated !"
      else
        echo "Error while updated !"
      fi
    else
      echo "No update required."
    fi
  else
    echo 'WAN IP not found.'
  fi
  sleep $DYNDNS_WAIT_DURATION
done